# Taller de seguretat i privacitat

Eines i comportaments per a protegir-te a tu i a qui t'envolta


#HSLIDE

###Criteris i filosofia

Concepte | Òptim | Subòptim | Inacceptable
---------|-------|----------|-------------
Privacitat|Per disseny|Per política|Cap
Programari|Codi obert| - |Privatiu
Xifrat|Extrem a extrem|Punt a punt|Cap
Sistema|Descentralitzat|Federat o centralitzat| -

#VSLIDE

Concepte | Òptim | Subòptim | Inacceptable
---------|-------|----------|-------------
Informació protegida|Dades i metadades|Només dades|Cap
Seguretat|Per defecte|Opcional|No segura
Model|Sense lucre|Amb lucre|Dades usuari

__Què vol dir tot això?__

#VSLIDE

### Privacitat per disseny vs. per política
* __Per disseny:__ El propi disseny del programa fa que les dades no es puguin accedir pel proveidor (Tor).
* __Per política:__ Les dades són accessibles pel proveïdor, però la seva política és no fer-ho, o esborrar-les (OpenMailbox, Riseup, Posteo...).

#VSLIDE

### Codi obert vs. Privatiu
* __Obert:__ El codi font del programa es públic, revisable i auditable per tothom (Firefox).
* __Privatiu:__ El codi font no és accessible i per tant no es pot revisar (Google Chrome).

#VSLIDE

### Xifrat extrem a extrem vs. punt a punt vs. en clar
Pregunta: Què vol dir xifrar?
* __Extrem a extrem:__ Les dades es xifren a la màquina de l'emisor i es desxifren a la del receptor (Signal).
* __Punt a punt:__ Les dades es xifren per viatjar d'una màquina a una altra pero es desxifren en punts intermedis (Telegram web, e-mail).
* __En clar:__ Les dades no es xifren en cap moment i viatgen "tal qual" (Whatsapp pre-2012).


#VSLIDE

### Descentralitzat vs. federat vs. centralitzat
* __Descentralitzat (P2P):__ Les maquines són client i servidor a la vegada, es comuniquen directament (Retroshare, Ricochet).
* __Federat:__ La comunicació depèn de servidors intermediaris, però no d'un únic proveïdor (e-mail, Matrix, Jabber).
* __Centralitzat:__ La comunicció depèn de servidors intermediaris i d'un únic proveïdor (Signal, Whatsapp, Telegram).

#VSLIDE

### Nivells de protecció de la informació
* __Dades i metadades:__ Terceres persones no poden accedir ni al contingut dels missatges ni a qui els envia o rep, ni quan (Ricochet).
* __Dades:__ Terceres persones no poden accedir al contingut dels missatges, però sí a altra informació com: emissor, receptor, hores i freqüència dels missatges, graf social, etc (Mail xifrat amb GPG).
* __Cap:__ Terceres persones poden accedir al contingut dels missatges i també a les metadades (e-mail).

#HSLIDE

### Missatgeria instantània: Signal
![Signal](signal/img/signal-logo.png)

#VSLIDE
#### Signal és
Un sistema de missatgeria instantània semblant a Whatsapp i a Telegram. Proporciona seguretat extrem a extrem i està gestionada per una associació sense ànim de lucre.
#VSLIDE

###PROS:
* Privacitat per disseny amb les dades.
* Xifrat extrem-a-extrem per text i multimèdia.
* Altament usable, semblant a Whatsapp.
* Codi i protocol de xifrat obert.
* Privacitat per defecte.
* Bloqueja els missatges si les claus de xifrat han canviat i les claus es poden comprobar fàcilment.

#VSLIDE

###CONTRES:
* Sistema 100% centralitzat.
* Les metadades no estan protegides per disseny, pero sí per política.
* No disponible fora de Google Play.
* Registre amb número de telèfon mòbil: no anònim, altament regulat

#VSLIDE
![Signal per Android](signal/img/signal-android.jpg)

#VSLIDE

##### Petició de Dades per part del FBI, publicada per OWS i ACLU (Octubre 2016)
![La petició del FBI](signal/img/subpoena-fbi-signal.png)
* OWS: Open Whisper Systems. Associació que desenvolupa i manté Signal.
* ACLU: American Civil Liberties Union. Organització a favor dels drets civils dels USA.
#VSLIDE

#### La informació proporcionada per OWS
![La informació que tenia OWS](signal/img/signal-subpoena-reply.png)

[Més informació](http://arstechnica.com/tech-policy/2016/10/fbi-demands-signal-user-data-but-theres-not-much-to-hand-over/) (en anglès)

#HSLIDE
### Anonimat a la xarxa: Tor
![Logo de Tor](tor/img/tor-logo.png)

#VSLIDE
#### Tor és
* Una **xarxa** d'ordinadors sobre internet per anonimitzar.
* El **programa** que connecta els ordinadors a la xarxa Tor.
* El **navegador** que funciona a través de la xarxa Tor.

#VSLIDE
#### Funcions
* Amaga l'adreça IP
* Xifra el trànsit
* Serveis ocults

#VSLIDE
#### Contrapartides
* Retard i ample de banda limitat
* Crida l'atenció
* Bloquejos, traves

#VSLIDE

#### Adreça IP

**Destí** i **remitent** d'una comunicació a internet.

El nostre remitent és una **adreça pública**.

Pot ser la de:
* casa
* biblioteca
* local social
* …

#VSLIDE
#### Adreça IP

Les ISP tenen comprats **rangs** d'adreces IP, i estan repartits per **zones** geogràfiques. Per tant, la nostra adreça IP pública revela:
* La companyia telefònica contractada
* La localització física aproximada

#VSLIDE
#### Adreça IP
Podem esbrinar la informació que escampem per internet amb serveis com [ipleak.net](https://ipleak.net)
![Adreça IP](tor/img/ip-leak_clearnet.gif)

#VSLIDE

#### Internet ≠ WWW
A Internet hi funcionen molts protocols d'aplicació. Per conviure en la mateixa màquina, es fan servir ports TCP o UDP.

La Web fa servir els següents ports
* HTTP: TCP-80
* HTTPS: TCP-443
* Resolució de noms (DNS): UDP-53

#VSLIDE
#### Internet ≠ WWW
Però n'hi ha molts més, de protocols
* Xat (Jabber): TCP-5222
* Correu: TCP-993, TCP-587, TCP-465
* Age of Empires II: TCP-47624

#VSLIDE
#### Internet ≠ WWW
Actualment la tendència és definir nous protocols per sobre de HTTP: les **API web**
* API de Twitter
* Xat (Matrix.org, RocketChat)
* Xarxes socials lliures (federació entre servidors)

#VSLIDE
#### Connexió HTTP, sense xifrar
![Connexió HTTP](tor/img/internet_16-9.png)

#VSLIDE

#### Connexió xifrada, TLS.
Xifra la connexió entre client i servidor.
* Web, mail, xat, …

Els túnels TLS ens protegeixen d'atacs:
* **Passius**: robar credencials, llegir formularis i missatges
* **Actius**: manipular webs, programes, actualitzacions.

#VSLIDE
#### Connexió HTTPS, xifrada

![Connexió HTTPS](tor/img/tls-internet_16-9.png)

#VSLIDE
#### Adreça IP del remitent **sense** usar **Tor**
Des de Barcelona i amb Movistar:
![Adreça IP sense Tor](tor/img/ip-leak_clearnet.gif)

#VSLIDE
#### Adreça IP del remitent **usant Tor**
Des de Barcelona i amb Movistar:
![Adreça IP amb Tor](tor/img/ip-leak_tor.gif)

#VSLIDE
#### Connexió a través de Tor
![Connexió amb Tor](tor/img/tor-internet_16-9.png)

#VSLIDE
#### Nodes de sortida a Clearnet

* Pocs: ample de banda limitat.
* Perillosos: Imprescindible HTTPS.

#VSLIDE
#### Connexió a través de Tor
![Connexió amb Tor i TLS](tor/img/tor-tls-internet_16-9.png)


#VSLIDE
#### Serveis ocults

Comunicació entre desconeguts
* Servidor anònim.
* Usuària anònima.

En comparació a serveis a la clearnet:
* No congestiona els nodes de sortida.
* No necessita HTTPS.

#VSLIDE
#### Alguns llocs web ocults

* Hidden Wiki: http://zqktlwi4fecvo6ri.onion/wiki/
* Críptica: http://cripticavraowaqb.onion
* Wikileaks: http://wlupld3ptjvsgwqw.onion/
* Webmail: http://sigaintevyh2rzvw.onion/

#VSLIDE
#### Aplicacions basades en serveis ocults
* OnionShare: compartir fitxers
* Ricochet: xat auster

#VSLIDE
#### Aplicacions torificables:
* Navegador: Firefox → Tor Browser Bundle
* Correu: Thunderbird → extensió TorBirdy
* App Twitter oficial → app Twidere + Orbot

#HSLIDE

### Anonimat multimèdia: ObscuraCam
![Obscuracam Banner](obscuracam/img/obscuracam_banner.png)


#VSLIDE
#### Funcions
* Amagar cares o objectes identificatius
* Esborrar les metadades EXIF de fotos i vídeos

#VSLIDE
#### Pixela cares
* Detecta automàticament les cares de la foto
* Permet afegir i modificar les censures

![Cares pixelades](obscuracam/img/cares_pixelades.png)

#VSLIDE
#### Metadades EXIF
Fotografia adorable...

![Nens menjant gelat](obscuracam/img/nens_gelat.jpg)

#VSLIDE
#### Metadades EXIF
... amb més informació del que sembla!
```
user@host:~$ exiftool nens_gelat.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
Camera Model Name : SM-G900V
ISO               : 64
Exif Version      : 0220
Flash             : No Flash
Aperture          : 2.2
GPS Altitude      : 18 m Below Sea Level
GPS Date/Time     : 2016:04:17 20:25:10Z
GPS Latitude      : 40 deg 42' 56.07" N
GPS Longitude     : 73 deg 50' 36.35" W
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
Shutter Speed     : 1/120
Create Date       : 2016:04:17 16:25:24.890
Focal Length      : 4.8 mm (35 mm equivalent: 31.0 mm)
```

#VSLIDE
#### Metadades EXIF: model de càmera
```
Camera Model Name : SM-G900V
```
![EXIF: Model de la càmera](obscuracam/img/exif_camera-model.png)

#VSLIDE
#### Metadades EXIF: ubicació
```
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
```
![EXIF: Ubicació](obscuracam/img/exif_gps.png)

#VSLIDE
#### Resultat metadades
Després de passar la foto per Obscuracam,
veiem que tota la informació compromesa ha estat esborrada.
```
user@host:~$ exiftool nens_gelat_neta.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
```
#VSLIDE
#### Resultat cares
_«Alguien ha pensado en los niños»_

![EXIF: pixelades](obscuracam/img/nens_gelat_neta.jpg)

#VSLIDE
#### Com instaŀlar i fer servir Obscuracam
* Security in a Box: [Android: Obscuracam](https://securityinabox.org/es/obscuracam)
* FLOSS Manuals: [Obscuring Photos](http://booki.flossmanuals.net/obscuracam/obscuring-photos)

#VSLIDE
#### Metadades: més info
* Què són les metadades: [wiki de "Metadata Anonymisation Toolkit"](https://mat.boum.org/)
* Eina exiftool: https://linux.die.net/man/1/exiftool

#VSLIDE
#### Reconeixement
* Nens adorables: [Joe Shlabotnik "Kids Eating Ice Cream On The School Steps" - CC by-nc-sa v2.0](https://www.flickr.com/photos/joeshlabotnik/31892072800/)
* Cares pixelades: [FLOSS Manuals Foundation - CC by-sa v3.0](https://flossmanuals.net)

#HSLIDE

### Complements per Firefox
La navegació per internet no és privada ni segura per defecte:
* Pàgines sense HTTPS per defecte
* Anuncis basats en els teus hàbits de navegació
* Cookies amb dates de caducitat molt llunyanes
* Programes que monitoren la teva activitat a les pàgines que visites

#VSLIDE
#### HTTPS Everywhere
![Logo HTTPS Everywhere](firefox-addons/img/https-everywhere.png)

* Força comunicació xifrada (HTTPS) si el servidor la suporta.
* [Descarrega'l](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
#VSLIDE

#### uBlock Origin
![Logo uBlock Origin](firefox-addons/img/ublock-origin.png)

* Bloqueja els anuncis de manera eficient i permet crear els teus propis filtres
* [Descarrega'l](https://addons.mozilla.org/en-us/firefox/addon/ublock-origin/)

#VSLIDE
#### Self-desructing Cookies
![Logo Self-destructing cookies](firefox-addons/img/self-destructing-cookies.jpeg)
* Destrueix les cookies de qualsevol pestanya que no tinguis oberta
* [Descarrega'l](https://addons.mozilla.org/en-US/firefox/addon/self-destructing-cookies/)

#VSLIDE
#### Privacy badger
![Logo Privacy Badger](firefox-addons/img/privacy-badger.png)

* Bloqueja els continguts de tercers que tinguin l'objectiu de rastrejar
* [Descarrega'l](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)

#HSLIDE

### Gestió de credencials: KeepassX
![KeepassX logo](keepassx/img/keepassx_logo.png)

#VSLIDE
#### Els 3 manaments de les contrasenyes
* _No les reutilitzaràs_
* _No les compartiràs_
* _No les faràs febles_

#VSLIDE
#### Propietats
* Caràcters diferents
* **Llargues**
* Difícils d'endevinar
    * Patrons evidents no
    * Dades conegudes no: noms, dates, aficions, etc.

#VSLIDE
#### Exemples
Exemples de menys a més fortes:
* Indigna: `tkmamor`
* Txepi-txepi: `tkmuchoMIam0r!<3`
* Idestructible:
  `mi corazon palpita como una patata frita tutum tutum`

#VSLIDE
#### Les matemàtiques darrere
Millor invertir en longitud que en caràcters diferents.

* **x**: Possibilitats per cada caràcter (només minúscules: 26)
* **y**: Longitud en caràcters
* **z**: Nombre total de possibles contrasenyes

  ![z = x^y](keepassx/img/possibilitats.png)
* **H**: Entropia (complexitat). Objectiu: més de 100 bits

   ![H = log2(z) = y*log2(x)](keepassx/img/entropia.png)

#VSLIDE
#### Representació en 3D
![gràfic longitud vs símbols](keepassx/img/entropia3d.png)

#VSLIDE
#### Gestió ideal de les contrasenyes
* Una per cada compte → moltes!
* Difícils d'endevinar → aleatòries!
* Difícils de trencar a "força bruta" → llargues!

#VSLIDE
#### Gestió habitual de les contrasenyes
* Una per TOTS els comptes
* Fàcil d'endevinar → aniversaris, noms propers, …
* Fàcil de trencar → pocs caràcters

**TENIM UN PROBLEMA**

#VSLIDE
#### Gestor de contrasenyes
* Base de dades xifrada
* Una contrasenya per dominar-les a totes
* Generador de contrasenyes fortes

#VSLIDE
#### Com fer-la servir
* Organització
    * per grups, subgrups i entrades
    * entrades desordenades i buscador
* Fer còpies de seguretat

#VSLIDE
* Tutorial a Surveillance Self Defense: [Cómo usar KeepassX](https://ssd.eff.org/es/module/c%C3%B3mo-usar-keepassx)
* Tutorial a Security in a Box: [KeePass – Almacenamiento seguro de contraseñas](https://securityinabox.org/es/keepass_main)
* Calculadora de robustesa de contrasenyes: [passwd.criptica.org](https://passwd.criptica.org)

#HSLIDE

#### Cercadors
Google ens rastreja
* Amb l'historial de cerca (Google)
* Amb l'historial de navegació (Chrome)
* Amb els anuncis en altres llocs (AdSites)
* Amb anàlisi per admins de webs (Analytics)
* …

#VSLIDE
#### Opcions respectuoses
* [DuckDuckGo](https://ddg.gg)
* [Startpage](https://startpage.com)
* [Disconnect](https://search.disconnect.me)

#HSLIDE

### Gràcies per la vostra atenció. Estem en contacte!
* https://criptica.org
* https://twitter.com/CripticaOrg
* https://quitter.cat/criptica
* Apunta't a la [llista de correu](https://www.autistici.org/mailman/listinfo/list_criptica)!
